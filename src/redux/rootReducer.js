import { combineReducers } from "redux";

import * as auth from "../app/modules/Auth/_redux/authRedux";
import * as demo from "../app/modules/_Demo/_redux/demoRedux";
import * as employee from "../app/modules/_EmployeeDemo/_redux/employeeRedux";
import * as propductGroup from "../app/modules/ProductGroup/_redux/productGroupRedux";
import * as product from "../app/modules/Product/_redux/productRedux";
import * as stock from "../app/modules/Stock/_redux/stockRedux";

export const rootReducer = combineReducers({
  auth: auth.reducer,
  demo: demo.reducer,
  employee: employee.reducer,
  productGroup: propductGroup.reducer,
  product: product.reducer,
  stock: stock.reducer,
});
