require("dayjs/locale/th");
var dayjs = require("dayjs");
dayjs.locale("th");

//http://uat.siamsmile.co.th:9188/swagger/index.html
//https://json-to-js.com/
// action type บอกว่า Redux ตัวนี้ สามารถทำอะไรได้บ้าง
export const actionTypes = {
  // ADD_PLAYER: '[Add player] Action',
  //SET_CURRENTPAGE: "[SET_CURRENTPAGE] Action",

  ADD_PRODUCTGROUPSEARCH: "[ADD_PRODUCTGROUPSEARCH] Action",
  ADD_PRODUCTGROUPEDIT: "[ADD_PRODUCTGROUPEDIT] Action",
  RESET_PRODUCTGROUPEDIT: "[RESET_PRODUCTGROUPEDIT] Action",
  SET_OPENMODAL: "[SET_OPENMODAL] Action",
  RESET_OPENMODAL: "[RESET_OPENMODAL] Action",

  ADD_PRODUCTGROUPTOUPDATE: "[ADD_PRODUCTGROUPTOUPDATE] Action",
};

// state ค่าที่ถูกเก็บไว้
const initialState = {
  productGroupSearch: {
    searchProductGroupStatus: 0,
    searchProductGroupName: "",
  },

  openmodal: {
    prductgroupId: 0,
    isopenmodal: false,
  },

  paginated: {
    page: 1,
    recordsPerPage: 10,
    orderingField: "",
    ascendingOrder: true,
  },

  productGroupForEdit: {
    ProductGroupId: 0,
    ProductGroupStatus: 0,
    ProductGroupName: "",
  },

  productGroupForUpdate: {
    id: 0,
    name: "",
    statusId: 0,
  },
};

// reducer แต่ละ Action จะไป update State อย่างไร
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_PRODUCTGROUPSEARCH: {
      return { ...state, productGroupSearch: action.payload };
    }
    case actionTypes.ADD_PRODUCTGROUPEDIT: {
      return { ...state, productGroupForEdit: action.payload };
    }

    case actionTypes.RESET_PRODUCTGROUPEDIT: {
      return {
        ...state,
        productGroupForEdit: initialState.productGroupForEdit,
      };
    }

    case actionTypes.SET_OPENMODAL: {
      return { ...state, openmodal: action.payload };
    }

    case actionTypes.RESET_OPENMODAL: {
      return {
        ...state,
        openmodal: initialState.openmodal,
      };
    }

    case actionTypes.ADD_PRODUCTGROUPTOUPDATE: {
      return { ...state, productGroupForUpdate: action.payload };
    }

    default:
      return state;
  }
};

//action เอาไว้เรียกจากข้างนอก เพื่อเปลี่ยน state
export const actions = {
  addProductGroupSearch: (payload) => ({
    type: actionTypes.ADD_PRODUCTGROUPSEARCH,
    payload,
  }),
  addProductGroupedit: (payload) => ({
    type: actionTypes.ADD_PRODUCTGROUPEDIT,
    payload,
  }),
  resetProductGroupedit: () => ({
    type: actionTypes.RESET_PRODUCTGROUPEDIT,
  }),

  resetOpenModal: () => ({
    type: actionTypes.RESET_OPENMODAL,
  }),

  setOpenModal: (payload) => ({
    type: actionTypes.SET_OPENMODAL,
    payload,
  }),

  addProductGroupToUpdate: (payload) => ({
    type: actionTypes.ADD_PRODUCTGROUPTOUPDATE,
    payload,
  }),
  //   updateCurrentEmployee: (payload) => ({ type: actionTypes.UPDATE_CURRENT_EMPLOYEE, payload }),
  //   resetCurrentEmployee: () => ({ type: actionTypes.RESET_EMPLOYEE }),
};
