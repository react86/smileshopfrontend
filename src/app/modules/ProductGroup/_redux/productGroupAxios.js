import axios from "axios";
import * as CONST from "../../../../Constants";
import { encodeURLWithParams } from "../../Common/components/ParamsEncode";

const ProductGroup_URL = `${CONST.API_URL}/ProductGroup`;

export const addProductGroup = (payload) => {
  return axios.post(`${ProductGroup_URL}/addproductGroup`, payload);
};

export const getProductGroupFilter = (
  orderingField,
  ascendingOrder,
  page,
  recordsPerPage,
  Name,
  StatusId
) => {
  debugger;
  let payload = {
    page,
    recordsPerPage,
    orderingField,
    ascendingOrder,
    Name,
    StatusId,
  };
  return axios.get(
    encodeURLWithParams(`${ProductGroup_URL}/getProductGroupFilter`, payload)
  );
};

export const getProductGroup = (id) => {
  return axios.get(`${ProductGroup_URL}/getProductGroupbyId/${id}`);
};

export const updateProductGroup = (payload) => {
  debugger;
  return axios.put(`${ProductGroup_URL}/updateProductGroup`, payload);
};
