/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import ProductGroupList from "../components/ProductGroupList";

import { Typography } from "@material-ui/core";

function ProductGroup(props) {
  return (
    <div>
      <Typography variant="h6" style={{ fontSize: "24px", color: "#424242" }}>
        จัดการข้อมูล ProductGroup
      </Typography>

      <ProductGroupList></ProductGroupList>
    </div>
  );
}

export default ProductGroup;
