/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */
import * as React from "react";
import { Formik, Field } from "formik";
import { Button, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import * as swal from "../../Common/components/SweetAlert";

import { TextField } from "formik-material-ui";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

import * as productGroupAxios from "../_redux/productGroupAxios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(1),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper_modal: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #e3f2fd",
    borderRadius: "5px",
    boxShadow: theme.shadows[5],
    width: 500,
  },
}));

function ProductGroupNew(props) {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = ({ setSubmitting }, objPayload) => {
    productGroupAxios
      .addProductGroup(objPayload)
      .then(async (res) => {
        if (res.data.isSuccess) {
          handleClose();
          //Swal
          swal
            .swalSuccess("Add Completed", `Add ProductGroup Success`)
            .then(() => {});
        }
      })
      .catch((err) => {
        //Swall Error
        swal.swalError("มีข้อผิดพลาด", err.Message);
      })
      .finally(() => {
        setSubmitting(false);
      });
  };

  return (
    <Formik
      //Form fields and default values
      initialValues={{
        Name: "",
      }}
      //Validation section
      validate={(values) => {
        const errors = {};

        if (!values.Name) {
          errors.Name = "Required";
        }

        return errors;
      }}
      //Form Submission
      // ต้องผ่าน Validate ก่อน ถึงจะถูกเรียก
      onSubmit={(values, { setSubmitting, resetForm }) => {
        handleSave({ setSubmitting }, values);
        resetForm(values);
        props.submit(values);
        setSubmitting(false);
      }}
      onReset={() => {
        handleClose();
      }}
    >
      {/* Render form */}
      {({
        submitForm,
        isSubmitting,
        values,
        errors,
        setFieldValue,
        touched,
        resetForm,
      }) => (
        <div>
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            onClick={handleOpen}
          >
            New ProductGroup
          </Button>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
            disableBackdropClick={true}
          >
            <Fade in={open}>
              <div className={classes.paper_modal}>
                <h2
                  id="transition-modal-title"
                  style={{
                    backgroundColor: "#0277bd",
                    color: "#fff",
                    padding: "12px",
                  }}
                >
                  Add ProductGroup
                </h2>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  <Grid item xs={12} lg={6}>
                    <Field
                      fullWidth
                      component={TextField}
                      required
                      type="text"
                      label=" Name"
                      name="Name"
                      errors={errors}
                    />
                  </Grid>
                </Grid>

                <p
                  id="transition-modal-description"
                  style={{ marginTop: "10px", textAlign: "center" }}
                >
                  <Button
                    disabled={isSubmitting}
                    onClick={submitForm}
                    variant="contained"
                    color="primary"
                    style={{ color: "#fff" }}
                  >
                    Save
                  </Button>
                  <Button
                    onClick={resetForm}
                    variant="contained"
                    style={{
                      backgroundColor: "#e53935",
                      marginLeft: "5px",
                      color: "#fff",
                    }}
                  >
                    Close
                  </Button>
                </p>
              </div>
            </Fade>
          </Modal>
        </div>
      )}
    </Formik>
  );
}

export default ProductGroupNew;
