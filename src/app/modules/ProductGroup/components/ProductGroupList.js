/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import { Grid, Typography, Chip, Icon } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import { useDispatch, useSelector } from "react-redux";
import * as productgroupRedux from "../_redux/productGroupRedux";
import * as productGroupAxios from "../_redux/productGroupAxios";
import ProductGroupSearch from "./ProductGroupSearch";
import ProductGroupEdit from "./ProductGroupEdit";

import EditButton from "../../Common/components/Buttons/EditButton";
import DoneIcon from "@material-ui/icons/Done";
import * as swal from "../../Common/components/SweetAlert";

var flatten = require("flat");

require("dayjs/locale/th");
var dayjs = require("dayjs");
dayjs.locale("th");

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(1),
  },
  paper_modal: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #e3f2fd",
    borderRadius: "5px",
    boxShadow: theme.shadows[5],
    width: 500,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

function ProductGroupList(props) {
  const classes = useStyles();

  const dispatch = useDispatch();
  const productGroupReducer = useSelector(({ productGroup }) => productGroup);

  const [paginated, setPaginated] = React.useState({
    page: 1,
    recordsPerPage: 5,
    orderingField: "",
    ascendingOrder: true,
    searchValues: {
      searchProductGroupStatus: 0,
      searchProductGroupName: "",
    },
  });

  const [totalRecords, setTotalRecords] = React.useState(0);

  const [data, setData] = React.useState([]);

  React.useEffect(() => {
    debugger;
    //Load Data
    loadData();
  }, [paginated]);

  const handleSearch = (values) => {
    debugger;
    setPaginated({
      ...paginated,
      page: 1,
      searchValues: {
        searchProductGroupStatus: (values.productGroupStatusId = ""
          ? 0
          : parseInt(values.productGroupStatusId)),
        searchProductGroupName: values.productGroupName,
      },
    });
  };

  const handleForAdd = (values) => {
    debugger;
    setPaginated({
      ...paginated,
      page: 1,
      searchValues: {
        searchProductGroupStatus: 0,
        searchProductGroupName: "",
      },
    });
  };

  const handleForEdit = (values) => {
    debugger;
    setPaginated({
      ...paginated,
      page: 1,
    });
  };

  const handleOpen = (pdgId) => {
    let objOpenModalPayload = {
      ...productGroupReducer.openmodal,
      prductgroupId: pdgId,
      isopenmodal: true,
    };
    dispatch(productgroupRedux.actions.setOpenModal(objOpenModalPayload));
  };

  const loadData = () => {
    productGroupAxios
      .getProductGroupFilter(
        paginated.orderingField,
        paginated.ascendingOrder,
        paginated.page,
        paginated.recordsPerPage,
        paginated.searchValues.searchProductGroupName,
        paginated.searchValues.searchProductGroupStatus
      )
      .then((res) => {
        if (res.data.isSuccess) {
          let flatData = [];
          res.data.data.forEach((element) => {
            flatData.push(flatten(element));
          });
          setData(flatData);

          setTotalRecords(res.data.totalAmountRecords);
        } else {
          alert(res.data.message);
          swal.swalError("มีข้อผิดพลาด", res.data.message);
        }
      })
      .catch((err) => {
        swal.swalError("มีข้อผิดพลาด", err.Message);
      });
  };

  const columns = [
    {
      name: "id",
      label: "รหัสรายการ",
    },
    {
      name: "name",
      label: "รายการ",
      option: {
        sort: false,
      },
    },
    {
      name: "statusId",
      label: "สถานะ",
      options: {
        customBodyRenderLite: (dataIndex, rowIndex) => {
          if (data[dataIndex].statusId === 1) {
            return (
              <Grid
                style={{ padding: 0, margin: 0 }}
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
              >
                <Chip
                  color="primary"
                  icon={<DoneIcon style={{ color: "#fff" }} />}
                  style={{ color: "#fff" }}
                  label="ใช้งาน"
                />
              </Grid>
            );
          } else {
            return (
              <Grid
                style={{ padding: 0, margin: 0 }}
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
              >
                <Chip
                  color="primary"
                  icon={
                    <Icon
                      style={{ backgroundColor: "#e57373", color: "#fff" }}
                      className="far fa-times-circle"
                    ></Icon>
                  }
                  style={{ backgroundColor: "#e57373", color: "#fff" }}
                  label="ไม่ใช้งาน"
                />
              </Grid>
            );
          }
        },
      },
    },

    {
      name: "วันที่สร้าง",
      options: {
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <Grid
              style={{ padding: 0, margin: 0 }}
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
            >
              {dayjs(data[dataIndex].createdDate).format("DD/MM/YYYY")}
            </Grid>
          );
        },
      },
    },
    {
      name: "",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <Grid
              style={{ padding: 0, margin: 0 }}
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
            >
              <EditButton
                onClick={() => {
                  handleOpen(data[dataIndex].id);
                }}
              >
                Edit
              </EditButton>
            </Grid>
          );
        },
      },
    },
  ];

  const options = {
    filterType: "checkbox",
    print: false,
    download: false,
    filter: false,
    search: false,
    selectableRows: "none",
    serverSide: true,
    count: totalRecords,

    page: paginated.page - 1,
    rowsPerPage: paginated.recordsPerPage,
    rowsPerPageOptions: [5, 10, 15, 20],
    responsive: "vertical",
    rowHover: true,
    onChangeRowsPerPage: (numberOfRows) => {
      setPaginated({ ...paginated, recordsPerPage: numberOfRows });
    },
    onChangePage: (currentPage) => {
      setPaginated({ ...paginated, page: currentPage + 1 });
    },
    onColumnSortChange: (changedColumn, direction) => {
      debugger;
      setPaginated({
        ...paginated,
        orderingField: `${changedColumn}`,
        ascendingOrder: direction === "asc" ? true : false,
      });
    },
    textLabels: {
      body: {
        noMatch: "ไม่พบข้อมูล",
        toolTip: "Sort",
        columnHeaderTooltip: (column) => `Sort for ${column.label}`,
      },
      pagination: {
        next: "ถัดไป",
        previous: "ย้อนกลับ",
        rowsPerPage: "ข้อมูลต่อหน้า",
        displayRows: "of",
      },
      viewColumns: {
        title: "แสดง Columns",
        titleAria: "Show/Hide Table Columns",
      },
    },
  };

  return (
    <div className={classes.root}>
      <ProductGroupSearch
        submit={handleSearch.bind(this)}
        p_name={paginated.searchValues.searchProductGroupName}
        p_status={paginated.searchValues.searchProductGroupStatus}
        newProductGroup={handleForAdd.bind(this)}
      ></ProductGroupSearch>
      <div style={{ paddingTop: "10px" }}>
        <MUIDataTable
          title={
            <Typography
              variant="h6"
              style={{ marginLeft: "6px", fontSize: "24px" }}
            >
              รายละเอียดข้อมูล
            </Typography>
          }
          data={data}
          columns={columns}
          options={options}
        />
      </div>

      <ProductGroupEdit submit={handleForEdit.bind(this)}></ProductGroupEdit>
    </div>
  );
}

export default ProductGroupList;
