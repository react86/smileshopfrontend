/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */
import * as React from "react";
import { Formik, Form, Field } from "formik";
import { Button, Grid, TextField } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import * as productgroupRedux from "../_redux/productGroupRedux";
import * as swal from "../../Common/components/SweetAlert";

import { Select } from "formik-material-ui";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

import * as productGroupAxios from "../_redux/productGroupAxios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(1),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper_modal: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #e3f2fd",
    borderRadius: "5px",
    boxShadow: theme.shadows[5],
    //padding: theme.spacing(2, 4, 3),
    width: 500,
  },
}));

function ProductGroupEdit(props) {
  const classes = useStyles();

  const dispatch = useDispatch();
  const productGroupReducer = useSelector(({ productGroup }) => productGroup);

  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    if (productGroupReducer.openmodal.isopenmodal === true) {
      handleOpen();
    }
  }, [productGroupReducer.openmodal]);

  React.useEffect(() => {
    debugger;
    if (productGroupReducer.productGroupForEdit.ProductGroupName !== "") {
      setOpen(true);
    } else {
      setOpen(false);
    }
  }, [productGroupReducer.productGroupForEdit]);

  const handleOpen = () => {
    productGroupAxios
      .getProductGroup(productGroupReducer.openmodal.prductgroupId)
      .then(async (res) => {
        if (res.data.isSuccess) {
          debugger;

          let objPayload = {
            ...productGroupReducer.productGroupForEdit,
            ProductGroupId: res.data.data.id,
            ProductGroupStatus: res.data.data.statusId,
            ProductGroupName: res.data.data.name,
          };

          dispatch(productgroupRedux.actions.addProductGroupedit(objPayload));
        }
      })
      .catch((err) => {
        //Swall Error
        swal.swalError("เกิดข้อผิดพลาด", err.Message);
      })
      .finally(() => {
        //setSubmitting(false);
      });
  };

  const handleSave = ({ setSubmitting }, values) => {
    let objPayload = {
      ...productGroupReducer.productGroupForUpdate,
      id: parseInt(productGroupReducer.productGroupForEdit.ProductGroupId),
      name: values.productGroupname,
      statusId: values.productGroupStatusId,
    };

    dispatch(productgroupRedux.actions.addProductGroupToUpdate(objPayload));

    productGroupAxios
      .updateProductGroup(objPayload)
      .then(async (res) => {
        if (res.data.isSuccess) {
          //Swal
          swal
            .swalSuccess("Edit Completed", `Edit ProductGroup Success`)
            .then(() => {});
        }
      })
      .catch((err) => {
        //Swall Error
        swal.swalError("เกิดข้อผิดพลาด", err.Message);
      })
      .finally(() => {
        props.submit(values);
        setSubmitting(false);
      });
  };

  const handleClose = ({ resetForm }, values) => {
    resetForm(values);
    dispatch(productgroupRedux.actions.resetOpenModal());
    dispatch(productgroupRedux.actions.resetProductGroupedit());
  };

  return (
    <Formik
      enableReinitialize
      //Form fields and default values
      initialValues={{
        productGroupname:
          productGroupReducer.productGroupForEdit.ProductGroupName,
        productGroupStatusId:
          productGroupReducer.productGroupForEdit.ProductGroupStatus,
      }}
      //Validation section
      validate={(values) => {
        const errors = {};

        if (!values.productGroupname) {
          errors.productGroupname = "Required";
        }

        return errors;
      }}
      //Form Submission
      // ต้องผ่าน Validate ก่อน ถึงจะถูกเรียก
      onSubmit={(values, { setSubmitting, resetForm }) => {
        handleSave({ setSubmitting }, values);

        handleClose({ resetForm }, values);

        setSubmitting(false);
      }}
    >
      {/* Render form */}
      {({
        submitForm,
        isSubmitting,
        values,
        errors,
        setFieldValue,
        touched,
        resetForm,
        handleChange,
        handleSubmit,
      }) => (
        <div>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
            disableBackdropClick={true}
          >
            <Fade in={open}>
              <Form>
                <div className={classes.paper_modal}>
                  <h2
                    id="transition-modal-title"
                    style={{
                      backgroundColor: "#0277bd",
                      color: "#fff",
                      padding: "12px",
                    }}
                  >
                    Edit ProductGroup
                  </h2>
                  <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <Grid item xs={12} lg={6}>
                      <Field
                        fullWidth
                        component={TextField}
                        type="text"
                        label=" Name"
                        name="productGroupname"
                        value={values.productGroupname}
                        onChange={(e) =>
                          setFieldValue("productGroupname", e.target.value)
                        }
                      ></Field>
                    </Grid>
                  </Grid>

                  <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <Grid item xs={12} lg={6}>
                      <FormControl fullWidth>
                        <InputLabel htmlFor="productGroupStatusId-simple">
                          ProductGroupStatus
                        </InputLabel>
                        <Field
                          component={Select}
                          name="productGroupStatusId"
                          inputProps={{
                            id: "productGroupStatusId-simple",
                          }}
                          value={values.productGroupStatusId}
                          onChange={(e) =>
                            setFieldValue(
                              "productGroupStatusId",
                              e.target.value
                            )
                          }
                        >
                          <MenuItem value={0}>ทั้งหมด</MenuItem>
                          <MenuItem value={1}>ใช้งาน</MenuItem>
                          <MenuItem value={2}>ไม่ใช้งาน</MenuItem>
                        </Field>
                      </FormControl>
                    </Grid>
                  </Grid>

                  <p
                    id="transition-modal-description"
                    style={{ marginTop: "10px", textAlign: "center" }}
                  >
                    <Button
                      disabled={isSubmitting}
                      onClick={submitForm}
                      variant="contained"
                      color="primary"
                      style={{
                        color: "#fff",
                      }}
                    >
                      Save
                    </Button>
                    <Button
                      onClick={() => {
                        handleClose({ resetForm }, values);
                      }}
                      variant="contained"
                      style={{
                        backgroundColor: "#e53935",
                        marginLeft: "5px",
                        color: "#fff",
                      }}
                    >
                      Close
                    </Button>
                  </p>
                </div>
              </Form>
            </Fade>
          </Modal>
        </div>
      )}
    </Formik>
  );
}

export default ProductGroupEdit;
