/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */
import * as React from "react";
import { Formik, Form, Field } from "formik";
import {
  Button,
  LinearProgress,
  Grid,
  Paper,
  Typography,
} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import { makeStyles } from "@material-ui/core/styles";

import ProductGroupNew from "./ProductGroupNew";

import { Select, TextField } from "formik-material-ui";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(2),
    height: "auto",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper_modal: {
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: 500,
  },
}));

function ProductGroupSearch(props) {
  const classes = useStyles();

  const handleAdd = (values) => {
    props.newProductGroup(values);
  };

  return (
    <Paper elevation={3} className={classes.paper}>
      <Formik
        //Form fields and default values
        enableReinitialize
        initialValues={{
          productGroupStatusId: props.p_status,
          productGroupName: props.p_name,
        }}
        //Validation section
        validate={(values) => {
          const errors = {};

          return errors;
        }}
        //Form Submission
        // ต้องผ่าน Validate ก่อน ถึงจะถูกเรียก
        onSubmit={(values, { setSubmitting }) => {
          props.submit(values);

          setSubmitting(false);
        }}
      >
        {/* Render form */}
        {({
          submitForm,
          isSubmitting,
          values,
          errors,
          setFieldValue,
          touched,
        }) => (
          <Form>
            <Grid container spacing={3}>
              <Typography
                variant="h6"
                style={{ marginLeft: "10px", fontSize: "24px" }}
              >
                ค้นหารายการ
              </Typography>
            </Grid>
            <Grid container spacing={3}>
              <Grid item xs={12} lg={6}>
                <FormControl fullWidth>
                  <InputLabel htmlFor="productGroupStatusId-simple">
                    สถานะการใช้งาน
                  </InputLabel>
                  <Field
                    component={Select}
                    name="productGroupStatusId"
                    inputProps={{
                      id: "productGroupStatusId-simple",
                    }}
                    value={values.productGroupStatusId}
                  >
                    <MenuItem value={0}>ทั้งหมด</MenuItem>
                    <MenuItem value={1}>ใช้งาน</MenuItem>
                    <MenuItem value={2}>ไม่ใช้งาน</MenuItem>
                  </Field>
                </FormControl>
              </Grid>
              <Grid item xs={12} lg={6}>
                <Field
                  fullWidth
                  component={TextField}
                  type="text"
                  label="ค้นหารายการ"
                  name="productGroupName"
                  value={values.productGroupName}
                />
              </Grid>

              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                style={{ marginTop: 10 }}
                spacing={2}
              >
                <Grid item xs={12} lg={3}>
                  {isSubmitting && <LinearProgress />}
                  <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={isSubmitting}
                    onClick={submitForm}
                  >
                    Search
                  </Button>
                </Grid>

                <Grid item xs={12} lg={3}>
                  <ProductGroupNew
                    submit={handleAdd.bind(this)}
                  ></ProductGroupNew>
                </Grid>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </Paper>
  );
}

export default ProductGroupSearch;
