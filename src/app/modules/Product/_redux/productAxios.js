import axios from "axios";
import * as CONST from "../../../../Constants";
import { encodeURLWithParams } from "../../Common/components/ParamsEncode";

const Product_URL = `${CONST.API_URL}/Product`;
const ProductGroup_URL = `${CONST.API_URL}/ProductGroup`;

export const Filter = (
  orderingField,
  ascendingOrder,
  page,
  recordsPerPage,
  ProductGroupId,
  StatusId,
  Name
) => {
  debugger;
  let payload = {
    page,
    recordsPerPage,
    orderingField,
    ascendingOrder,
    Name,
  };

  if (ProductGroupId !== "") {
    payload.ProductGroupId = ProductGroupId;
  }
  if (StatusId !== "") {
    payload.StatusId = StatusId;
  }

  return axios.get(encodeURLWithParams(`${Product_URL}/filter`, payload));
};

export const addProduct = (payload) => {
  debugger;
  return axios.post(`${Product_URL}/addproduct`, payload);
};

export const getProductGroup = () => {
  debugger;
  return axios.get(`${ProductGroup_URL}/getAllProductGroup`);
};

export const getProductByProductGroupId = (productGroupId) => {
  debugger;
  return axios.get(`${Product_URL}/getproductByGroupId/${productGroupId}`);
};

// POST
// ​/api​/Product​/addproduct

// PUT
// ​/api​/Product​/getproduct​/{productId}

// PUT
// ​/api​/Product​/updateproduct

// GET
// ​/api​/Product​/filter
