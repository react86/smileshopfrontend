require("dayjs/locale/th");
var dayjs = require("dayjs");
dayjs.locale("th");

export const actionTypes = {
  // ADD_PLAYER: '[Add player] Action',
  //SET_CURRENTPAGE: "[SET_CURRENTPAGE] Action",

  SET_PAGINATED: "[SET_PAGINATED] Action",
  ADD_PRODUCTDATA: "[ADD_PRODUCTDATA] Action",
  RESET_PAGINATED: "[RESET_PAGINATED] Action",
};

// state ค่าที่ถูกเก็บไว้
const initialState = {
  paginated: {
    page: 1,
    recordsPerPage: 10,
    orderingField: "",
    ascendingOrder: true,
    productGroupId: 0,
    productStatusId: 0,
    productname: "",
  },

  productdata: {
    name: "",
    price: 0,
    stock: 0,
    productGroupId: 0,
  },
};

// reducer แต่ละ Action จะไป update State อย่างไร
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_PAGINATED: {
      return { ...state, paginated: action.payload };
    }
    case actionTypes.ADD_PRODUCTDATA: {
      return { ...state, productdata: action.payload };
    }

    case actionTypes.RESET_PAGINATED: {
      return { ...state, paginated: initialState.paginated };
    }

    default:
      return state;
  }
};

//action เอาไว้เรียกจากข้างนอก เพื่อเปลี่ยน state
export const actions = {
  setPaginated: (payload) => ({
    type: actionTypes.SET_PAGINATED,
    payload,
  }),

  addProductData: (payload) => ({
    type: actionTypes.ADD_PRODUCTDATA,
    payload,
  }),

  resetPaginated: () => ({
    type: actionTypes.RESET_PAGINATED,
  }),
};
