import React from "react";
import ProductList from "../components/ProductList";
import ProductSearch from "../components/ProductSearch";

function Product(props) {
  return (
    <div>
      <p>Product</p>
      <ProductSearch></ProductSearch>
      <ProductList></ProductList>
    </div>
  );
}

export default Product;
