/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  useMediaQuery,
  Grid,
  TextField,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useFormik } from "formik";
import DropDown from "../../Common/components/DropDown/DropDown";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import * as productAxios from "../_redux/productAxios";
import { useDispatch, useSelector } from "react-redux";
import * as productRedux from "../_redux/productRedux";
import * as swal from "../../Common/components/SweetAlert";
import * as ProductAxios from "../_redux/productAxios";

function ProductNew(props) {
  const productReducer = useSelector(({ product }) => product);
  const dispatch = useDispatch();

  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const [productGroupData, setProductGroupData] = React.useState([]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    formik.resetForm();
    setOpen(false);
  };

  React.useEffect(() => {
    loadProductGroup();
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      productGroupId: 0,
      name: "",
      price: 0,
      stock: 0,
    },
    validate: (values) => {
      const errors = {};

      // if (!values.productGroupId) {
      //   errors.productGroupId = "Required";
      // }

      return errors;
    },
    onSubmit: (values) => {
      handleSave();
    },
  });

  const loadProductGroup = () => {
    ProductAxios.getProductGroup()
      .then((res) => {
        if (res.data.isSuccess) {
          setProductGroupData(res.data.data);
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const handleSave = () => {
    let values = formik.values;

    let objPayload = {
      ...productReducer.productdata,
      name: values.name,
      price: values.price,
      stock: values.stock,
      productGroupId: values.productGroupId,
    };

    dispatch(productRedux.actions.addProductData(objPayload));

    productAxios
      .addProduct(objPayload)
      .then((res) => {
        if (res.data.isSuccess) {
          swal
            .swalSuccess("Add Completed", "Add Product Success")
            .then(() => {});
        } else {
          swal.swalError(res.data.message).then(() => {});
        }
      })
      .catch((err) => {
        swal.swalError(err.message).then(() => {});
      })
      .finally(() => {
        props.submit(true);
        handleClose();
      });
  };

  return (
    <div>
      <Button
        variant="contained"
        color="secondary"
        fullWidth
        onClick={handleClickOpen}
      >
        New Product
      </Button>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        disableBackdropClick={true}
      >
        <DialogTitle id="responsive-dialog-title">{"Add Product"}</DialogTitle>
        <DialogContent>
          <Grid container direction="row" justify="center" alignItems="center">
            <Grid item xs={12}>
              <DropDown
                name="productGroupId"
                label="productGroupId"
                formik={formik}
                defaultMenu={true}
                defaultValue={formik.values.productGroupId}
                data={productGroupData}
              ></DropDown>
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="name"
                label="ชื่อ"
                fullWidth
                value={formik.values.name}
                onChange={formik.handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor="standard-adornment-amount">
                  ราคาสินค้า
                </InputLabel>
                <Input
                  type="number"
                  startAdornment={
                    <InputAdornment position="start">฿</InputAdornment>
                  }
                  name="price"
                  value={formik.values.price}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor="standard-adornment-amount">
                  จำนวนสินค้า
                </InputLabel>
                <Input
                  type="number"
                  name="stock"
                  value={formik.values.stock}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Grid>
          </Grid>
          {/* {JSON.stringify(formik.values)} */}
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={formik.handleSubmit} color="primary">
            ตกลง
          </Button>
          <Button onClick={handleClose} style={{ color: "#e53935" }} autoFocus>
            ยกเลิก
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default ProductNew;
