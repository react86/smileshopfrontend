/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import { useFormik } from "formik";
import { TextField, Button, Paper, Grid, MenuItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DropDown from "../../Common/components/DropDown/DropDown";
import { useDispatch, useSelector } from "react-redux";
import * as productRedux from "../_redux/productRedux";
import ProductNew from "./ProductNew";
import * as ProductAxios from "../_redux/productAxios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    height: "auto",
  },
  paper: {
    width: "100%",
    height: "auto",
    padding: theme.spacing(1),
  },
}));

function ProductSearch() {
  const classes = useStyles();

  const productReducer = useSelector(({ product }) => product);
  const dispatch = useDispatch();

  const [productGroupData, setProductGroupData] = React.useState([]);

  React.useEffect(() => {
    loadProductGroup();
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      statusId: 0,
      productGroupId: 0,
      keyword: "",
    },
    validate: (values) => {
      const errors = {};

      // if (!values.productGroupId) {
      //   errors.productGroupId = "Required";
      // }

      return errors;
    },
    onSubmit: (values) => {
      //alert(JSON.stringify(values, null, 2));
      let objpayload = {
        ...productReducer.paginated,
        page: 1,
        recordsPerPage: 10,
        orderingField: "",
        ascendingOrder: true,
        productGroupId: parseInt(values.productGroupId),
        productStatusId: parseInt(values.statusId),
        productname: values.keyword,
      };

      dispatch(productRedux.actions.setPaginated(objpayload));
    },
  });

  const loadProductGroup = () => {
    ProductAxios.getProductGroup()
      .then((res) => {
        if (res.data.isSuccess) {
          setProductGroupData(res.data.data);
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const handleAdd = () => {
    formik.resetForm();
    dispatch(productRedux.actions.resetPaginated());
  };

  return (
    <div className={classes.root}>
      <Paper elevation={3} className={classes.paper}>
        <Grid container spacing={3}>
          <Grid item xs={8} sm={4}>
            <DropDown
              name="productGroupId"
              label="productGroupId"
              formik={formik}
              defaultMenu={false}
              defaultValue={formik.values.productGroupId}
              defaultselect="ทั้งหมด"
              data={productGroupData}
            ></DropDown>
          </Grid>

          <Grid item xs={8} sm={4}>
            <TextField
              name="statusId"
              label="สถานะการใช้งาน"
              fullWidth
              select
              value={formik.values.statusId}
              onChange={formik.handleChange}
              error={formik.touched.statusId && Boolean(formik.errors.statusId)}
              helperText={formik.touched.statusId && formik.errors.statusId}
            >
              <MenuItem value={0}>ทั้งหมด</MenuItem>
              <MenuItem value={1}>ใช้งาน</MenuItem>
              <MenuItem value={2}>ไม่ใช้งาน</MenuItem>
            </TextField>
          </Grid>
          <Grid item xs={8} sm={4}>
            <TextField
              name="keyword"
              label="ค้นหา"
              fullWidth
              value={formik.values.keyword}
              onChange={formik.handleChange}
              error={formik.touched.keyword && Boolean(formik.errors.keyword)}
              helperText={formik.touched.keyword && formik.errors.keyword}
            ></TextField>
          </Grid>
        </Grid>

        <Grid
          container
          spacing={2}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={12} lg={3}>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              onClick={formik.handleSubmit}
            >
              ค้นหา
            </Button>
          </Grid>
          <Grid item xs={12} lg={3}>
            <ProductNew submit={handleAdd.bind(this)}></ProductNew>
          </Grid>
        </Grid>
        {JSON.stringify(formik.values)}
      </Paper>
    </div>
  );
}

export default ProductSearch;
