/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import { Grid, Typography, Chip, Icon } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import { useDispatch, useSelector } from "react-redux";
import * as productRedux from "../_redux/productRedux";
import * as productAxios from "../_redux/productAxios";
import * as swal from "../../Common/components/SweetAlert";
import DoneIcon from "@material-ui/icons/Done";

var flatten = require("flat");

require("dayjs/locale/th");
var dayjs = require("dayjs");
dayjs.locale("th");

function ProductList() {
  const productReducer = useSelector(({ product }) => product);
  const dispatch = useDispatch();

  const [totalRecords, setTotalRecords] = React.useState(0);

  const [data, setData] = React.useState([]);

  const [paginated, setPaginated] = React.useState({
    page: 1,
    recordsPerPage: 10,
    orderingField: "",
    ascendingOrder: true,
  });

  React.useEffect(() => {
    debugger;
    let objpayload = {
      ...productReducer.paginated,
      page: paginated.page,
      recordsPerPage: paginated.recordsPerPage,
      orderingField: paginated.orderingField,
      ascendingOrder: paginated.ascendingOrder,
    };

    dispatch(productRedux.actions.setPaginated(objpayload));
  }, [paginated]);

  React.useEffect(() => {
    debugger;
    //Load Data
    loadData();
    //alert(JSON.stringify(productReducer.paginated.productGroupId));
  }, [productReducer.paginated]);

  const loadData = () => {
    productAxios
      .Filter(
        productReducer.paginated.orderingField,
        productReducer.paginated.ascendingOrder,
        productReducer.paginated.page,
        productReducer.paginated.recordsPerPage,
        productReducer.paginated.productGroupId,
        productReducer.paginated.productStatusId,
        productReducer.paginated.productname
      )
      .then((res) => {
        if (res.data.isSuccess) {
          let flatData = [];
          res.data.data.forEach((element) => {
            flatData.push(flatten(element));
          });
          setData(flatData);

          setTotalRecords(res.data.totalAmountRecords);
        } else {
          alert(res.data.message);
          swal.swalError("มีข้อผิดพลาด", res.data.message);
        }
      })
      .catch((err) => {
        swal.swalError("มีข้อผิดพลาด", err.Message);
      });
  };

  const columns = [
    {
      name: "id",
      label: "รหัสรายการ",
    },
    {
      name: "name",
      label: "รายการ",
      option: {
        sort: false,
      },
    },
    {
      name: "productGroup.name",
      label: "ประเภทสินทค้า",
      option: {
        sort: false,
      },
    },
    {
      name: "price",
      label: "ราคา",
      option: {
        sort: false,
      },
    },
    {
      name: "price",
      label: "ราคา",
      option: {
        sort: false,
      },
    },
    {
      name: "stock",
      label: "จำนวนสินค้า",
      option: {
        sort: false,
      },
    },

    {
      name: "statusId",
      label: "สถานะ",
      options: {
        customBodyRenderLite: (dataIndex, rowIndex) => {
          if (data[dataIndex].statusId === 1) {
            return (
              <Grid
                style={{ padding: 0, margin: 0 }}
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
              >
                <Chip
                  color="primary"
                  icon={<DoneIcon style={{ color: "#fff" }} />}
                  style={{ color: "#fff" }}
                  label="ใช้งาน"
                />
              </Grid>
            );
          } else {
            return (
              <Grid
                style={{ padding: 0, margin: 0 }}
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
              >
                <Chip
                  color="primary"
                  icon={
                    <Icon
                      style={{ backgroundColor: "#e57373", color: "#fff" }}
                      className="far fa-times-circle"
                    ></Icon>
                  }
                  style={{ backgroundColor: "#e57373", color: "#fff" }}
                  label="ไม่ใช้งาน"
                />
              </Grid>
            );
          }
        },
      },
    },

    {
      name: "วันที่สร้าง",
      options: {
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <Grid
              style={{ padding: 0, margin: 0 }}
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
            >
              {dayjs(data[dataIndex].createdDate).format("DD/MM/YYYY")}
            </Grid>
          );
        },
      },
    },
    {
      name: "",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <Grid
              style={{ padding: 0, margin: 0 }}
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
            >
              {/* <EditButton
                onClick={() => {
                  handleOpen(data[dataIndex].id);
                }}
              >
                Edit
              </EditButton> */}
            </Grid>
          );
        },
      },
    },
  ];

  const options = {
    filterType: "checkbox",
    print: false,
    download: false,
    filter: false,
    search: false,
    selectableRows: "none",
    serverSide: true,
    count: totalRecords,

    page: productReducer.paginated.page - 1,
    rowsPerPage: productReducer.paginated.recordsPerPage,
    rowsPerPageOptions: [5, 10, 15, 20],
    responsive: "vertical",
    rowHover: true,
    onChangeRowsPerPage: (numberOfRows) => {
      setPaginated({ ...paginated, recordsPerPage: numberOfRows });
    },
    onChangePage: (currentPage) => {
      setPaginated({ ...paginated, page: currentPage + 1 });
    },
    onColumnSortChange: (changedColumn, direction) => {
      debugger;
      setPaginated({
        ...paginated,
        orderingField: `${changedColumn}`,
        ascendingOrder: direction === "asc" ? true : false,
      });
    },
    textLabels: {
      body: {
        noMatch: "ไม่พบข้อมูล",
        toolTip: "Sort",
        columnHeaderTooltip: (column) => `Sort for ${column.label}`,
      },
      pagination: {
        next: "ถัดไป",
        previous: "ย้อนกลับ",
        rowsPerPage: "ข้อมูลต่อหน้า",
        displayRows: "of",
      },
      viewColumns: {
        title: "แสดง Columns",
        titleAria: "Show/Hide Table Columns",
      },
    },
  };

  return (
    <div>
      <div style={{ paddingTop: "10px" }}>
        <MUIDataTable
          title={
            <Typography
              variant="h6"
              style={{ marginLeft: "6px", fontSize: "24px" }}
            >
              รายละเอียดข้อมูล
            </Typography>
          }
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    </div>
  );
}

export default ProductList;
