import React from "react";
import StockList from "../components/StockList";
import StockSearch from "../components/StockSearch";

function Stock() {
  return (
    <div>
      <p>Stock</p>
      <StockSearch></StockSearch>
      <StockList></StockList>
    </div>
  );
}

export default Stock;
