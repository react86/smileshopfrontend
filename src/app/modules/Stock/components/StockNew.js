/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  useMediaQuery,
  Grid,
  TextField,
  MenuItem,
} from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import { useFormik } from "formik";
import DropDown from "../../Common/components/DropDown/DropDown";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import { useDispatch, useSelector } from "react-redux";
import * as ProductAxios from "../../Product/_redux/productAxios";
import * as StockAxios from "../_redux/stockAxios";
import * as StockRedux from "../_redux/stockRedux";
import * as swal from "../../Common/components/SweetAlert";

function StockNew(props) {
  const dispatch = useDispatch();
  const stockReducer = useSelector(({ stock }) => stock);

  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const [productGroupData, setProductGroupData] = React.useState([]);
  const [productData, setProductData] = React.useState([]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    formik.resetForm();
    dispatch(StockRedux.actions.resetStockDetail());
    setOpen(false);
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      productGroupId: stockReducer.stockDetail.productGroupId,
      productId: stockReducer.stockDetail.productId,
      stockType: stockReducer.stockDetail.stockTypeId,
      amountStock: stockReducer.stockDetail.amountStock,
      amountBefore: stockReducer.stockDetail.amountBefore,
      amountAfter: stockReducer.stockDetail.amountAfter,
      remark: stockReducer.stockDetail.remark,
    },
    validate: (values) => {
      const errors = {};

      // if (!values.productGroupId) {
      //   errors.productGroupId = "Required";
      // }

      return errors;
    },
    onSubmit: (values) => {
      //handleSave();
      handleSave();
    },
  });

  React.useEffect(() => {
    loadProductGroup();
  }, []);

  React.useEffect(() => {
    formik.values.productId = 0;
    if (formik.values.productGroupId !== 0) {
      loadProductByProductGroupId();
    } else {
      setProductData([]);
    }
  }, [formik.values.productGroupId]);

  React.useEffect(() => {
    if (formik.values.productId !== 0) {
      //handleChange
      handleChange();
    } else {
      //handleReset
      handleReset();
    }
  }, [formik.values.productId]);

  React.useEffect(() => {
    //handleCalAmountAfter
    if (formik.values.stockType !== 0) {
      handleCalAmountAfter();
    }
  }, [formik.values.amountStock, formik.values.stockType]);

  const handleCalAmountAfter = () => {
    let typeId = formik.values.stockType;
    let amountStock = formik.values.amountStock;
    let amountBefore = formik.values.amountBefore;
    let amountAfter = 0;

    if (typeId === 1) {
      amountAfter = amountBefore + amountStock;
    } else {
      amountAfter = amountBefore - amountStock;
    }

    formik.values.amountAfter = amountAfter;
  };

  const handleChange = () => {
    StockAxios.getProduct(formik.values.productId)
      .then((res) => {
        if (res.data.isSuccess) {
          //setProductGroupData(res.data.data);
          let objPayload = {
            ...stockReducer.stockDetail,
            productGroupId: formik.values.productGroupId,
            productId: formik.values.productId,
            amountBefore: res.data.data.stock,
          };
          dispatch(StockRedux.actions.addStockDetail(objPayload));
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const handleReset = () => {
    let values = formik.values;

    values.stockType = 0;
    values.amountStock = 0;
    values.amountBefore = 0;
    values.amountAfter = 0;
    values.remark = "";
  };

  const loadProductGroup = () => {
    ProductAxios.getProductGroup()
      .then((res) => {
        if (res.data.isSuccess) {
          setProductGroupData(res.data.data);
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const loadProductByProductGroupId = () => {
    ProductAxios.getProductByProductGroupId(formik.values.productGroupId)
      .then((res) => {
        debugger;
        if (res.data.isSuccess) {
          setProductData(res.data.data);
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const handleSave = () => {
    //update values to Redux
    let objPayload = {
      ...stockReducer.stockDetail,
      productGroupId: formik.values.productGroupId,
      productId: formik.values.productId,
      stockTypeId: formik.values.stockType,
      amountStock: formik.values.amountStock,
      amountBefore: formik.values.amountBefore,
      amountAfter: formik.values.amountAfter,
      remark: formik.values.remark,
    };
    //Add Stock
    StockAxios.addStock(objPayload)
      .then((res) => {
        if (res.data.isSuccess) {
          swal.swalSuccess("Add Completed", "Add Stock Success").then(() => {});
        } else {
          swal.swalError(res.data.message).then(() => {});
        }
      })
      .catch((err) => {
        swal.swalError(err.message).then(() => {});
      })
      .finally(() => {
        handleClose();
        props.submit(true);
      });
  };

  return (
    <div>
      <Button
        variant="contained"
        color="secondary"
        fullWidth
        onClick={handleClickOpen}
      >
        New Stock
      </Button>

      <Dialog
        fullScreen={fullScreen}
        open={open}
        aria-labelledby="responsive-dialog-title"
        disableBackdropClick={true}
      >
        <DialogTitle id="responsive-dialog-title">{"Add Stock"}</DialogTitle>
        <DialogContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <DropDown
                name="productGroupId"
                label="productGroupId"
                formik={formik}
                defaultMenu={true}
                defaultValue={formik.values.productGroupId}
                //defaultselect="ทั้งหมด"
                data={productGroupData}
              ></DropDown>
            </Grid>

            <Grid item xs={12}>
              <DropDown
                name="productId"
                label="productId"
                formik={formik}
                defaultMenu={true}
                defaultValue={formik.values.productId}
                //defaultselect="ทั้งหมด"
                data={productData}
              ></DropDown>
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="stockType"
                label="ประเภท"
                fullWidth
                select
                value={formik.values.stockType}
                onChange={formik.handleChange}
                error={
                  formik.touched.stockType && Boolean(formik.errors.stockType)
                }
                helperText={formik.touched.stockType && formik.errors.stockType}
              >
                <MenuItem disabled value={0}>
                  โปรดเลือก
                </MenuItem>
                <MenuItem value={1}>เพิ่มสินค้า</MenuItem>
                <MenuItem value={2}>ลดสินค้า</MenuItem>
              </TextField>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor="standard-adornment-amount">
                  จำนวนสินค้า
                </InputLabel>
                <Input
                  type="number"
                  name="amountStock"
                  value={formik.values.amountStock}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth disabled>
                <InputLabel htmlFor="standard-adornment-amount">
                  จำนวนสินค้าคงเหลือ
                </InputLabel>
                <Input
                  type="number"
                  name="amountBefore"
                  value={formik.values.amountBefore}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth disabled>
                <InputLabel htmlFor="standard-adornment-amount">
                  จำนวนสินค้าล่าสุด
                </InputLabel>
                <Input
                  type="number"
                  name="amountAfter"
                  value={formik.values.amountAfter}
                  onChange={formik.handleChange}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                name="remark"
                label="หมายเหตุ"
                fullWidth
                multiline
                rowsMax={4}
                value={formik.values.remark}
                onChange={formik.handleChange}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={formik.handleSubmit} color="primary">
            ตกลง
          </Button>
          <Button onClick={handleClose} style={{ color: "#e53935" }} autoFocus>
            ยกเลิก
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default StockNew;
