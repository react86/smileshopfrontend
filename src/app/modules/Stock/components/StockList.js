/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import { Grid, Typography, Chip, Icon } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import { useDispatch, useSelector } from "react-redux";
import * as swal from "../../Common/components/SweetAlert";
import DoneIcon from "@material-ui/icons/Done";
import * as stockRedux from "../_redux/stockRedux";
import * as stockAxios from "../_redux/stockAxios";

var flatten = require("flat");

require("dayjs/locale/th");
var dayjs = require("dayjs");
dayjs.locale("th");

function StockList() {
  const stockReducer = useSelector(({ stock }) => stock);
  const dispatch = useDispatch();

  const [totalRecords, setTotalRecords] = React.useState(0);

  const [data, setData] = React.useState([]);

  const [paginated, setPaginated] = React.useState({
    page: 1,
    recordsPerPage: 10,
    orderingField: "",
    ascendingOrder: true,
  });

  React.useEffect(() => {
    debugger;
    let objpayload1 = {
      ...stockReducer.stockPaginated,
      page: paginated.page,
      recordsPerPage: paginated.recordsPerPage,
      orderingField: paginated.orderingField,
      ascendingOrder: paginated.ascendingOrder,
    };

    dispatch(stockRedux.actions.setStockPaginated(objpayload1));
  }, [paginated]);

  React.useEffect(() => {
    debugger;
    //Load Data
    loadData();
  }, [stockReducer.stockPaginated]);

  const loadData = () => {
    stockAxios
      .Filter(
        stockReducer.stockPaginated.orderingField,
        stockReducer.stockPaginated.ascendingOrder,
        stockReducer.stockPaginated.page,
        stockReducer.stockPaginated.recordsPerPage,
        stockReducer.stockPaginated.productGroupId,
        stockReducer.stockPaginated.productId
      )
      .then((res) => {
        if (res.data.isSuccess) {
          let flatData = [];
          res.data.data.forEach((element) => {
            flatData.push(flatten(element));
          });
          setData(flatData);

          setTotalRecords(res.data.totalAmountRecords);
        } else {
          alert(res.data.message);
          swal.swalError("มีข้อผิดพลาด", res.data.message);
        }
      })
      .catch((err) => {
        swal.swalError("มีข้อผิดพลาด", err.Message);
      });
  };

  const columns = [
    {
      name: "id",
      label: "รหัสรายการ",
    },
    {
      name: "createdDate",
      label: "วันที่สร้าง",
      options: {
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <Grid
              style={{ padding: 0, margin: 0 }}
              container
              direction="row"
              justify="flex-start"
              alignItems="center"
            >
              {dayjs(data[dataIndex].createdDate).format("DD/MM/YYYY")}
            </Grid>
          );
        },
      },
    },
    {
      name: "product.productGroup.name",
      label: "ประเภทสินค้า",
      option: {
        sort: false,
      },
    },

    {
      name: "product.name",
      label: "Product",
      option: {
        sort: false,
      },
    },

    {
      name: "amountBefore",
      label: "AmountBefore",
      option: {
        sort: false,
      },
    },
    {
      name: "amountStock",
      label: "AmountStock",
      options: {
        customBodyRenderLite: (dataIndex, rowIndex) => {
          debugger;
          if (data[dataIndex].stockTypeId === 1) {
            return (
              <Grid
                style={{ padding: 0, margin: 0 }}
                container
                direction="row"
                justify="center"
                alignItems="center"
              >
                + {data[dataIndex].amountStock}
              </Grid>
            );
          } else {
            return (
              <Grid
                style={{ padding: 0, margin: 0 }}
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
              >
                - {data[dataIndex].amountStock}
              </Grid>
            );
          }
        },
      },
    },
    {
      name: "amountAfter",
      label: "AmountAfter",
      option: {
        sort: false,
      },
    },
    {
      name: "remark",
      label: "หมายเหตุ",
      option: {
        sort: false,
      },
    },
    {
      name: "",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex, rowIndex) => {
          return (
            <Grid
              style={{ padding: 0, margin: 0 }}
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
            >
              {/* <EditButton
                  onClick={() => {
                    handleOpen(data[dataIndex].id);
                  }}
                >
                  Edit
                </EditButton> */}
            </Grid>
          );
        },
      },
    },
  ];

  const options = {
    filterType: "checkbox",
    print: false,
    download: false,
    filter: false,
    search: false,
    selectableRows: "none",
    serverSide: true,
    count: totalRecords,

    page: stockReducer.stockPaginated.page - 1,
    rowsPerPage: stockReducer.stockPaginated.recordsPerPage,
    rowsPerPageOptions: [5, 10, 15, 20],
    responsive: "vertical",
    rowHover: true,
    onChangeRowsPerPage: (numberOfRows) => {
      setPaginated({ ...paginated, recordsPerPage: numberOfRows });
    },
    onChangePage: (currentPage) => {
      setPaginated({ ...paginated, page: currentPage + 1 });
    },
    onColumnSortChange: (changedColumn, direction) => {
      debugger;
      setPaginated({
        ...paginated,
        orderingField: `${changedColumn}`,
        ascendingOrder: direction === "asc" ? true : false,
      });
    },
    textLabels: {
      body: {
        noMatch: "ไม่พบข้อมูล",
        toolTip: "Sort",
        columnHeaderTooltip: (column) => `Sort for ${column.label}`,
      },
      pagination: {
        next: "ถัดไป",
        previous: "ย้อนกลับ",
        rowsPerPage: "ข้อมูลต่อหน้า",
        displayRows: "of",
      },
      viewColumns: {
        title: "แสดง Columns",
        titleAria: "Show/Hide Table Columns",
      },
    },
  };

  return (
    <div>
      <div style={{ paddingTop: "10px" }}>
        <MUIDataTable
          title={
            <Typography
              variant="h6"
              style={{ marginLeft: "6px", fontSize: "24px" }}
            >
              รายละเอียดข้อมูล
            </Typography>
          }
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    </div>
  );
}

export default StockList;
