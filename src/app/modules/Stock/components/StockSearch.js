/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import { useFormik } from "formik";
import { TextField, Button, Paper, Grid, MenuItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DropDown from "../../Common/components/DropDown/DropDown";
import * as ProductAxios from "../../Product/_redux/productAxios";
import { useDispatch, useSelector } from "react-redux";
import * as stockRedux from "../_redux/stockRedux";
import StockNew from "./StockNew";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: "100%",
    height: "auto",
  },
  paper: {
    width: "100%",
    height: "auto",
    padding: theme.spacing(1),
  },
}));

function StockSearch() {
  const classes = useStyles();

  const stockReducer = useSelector(({ stock }) => stock);
  const dispatch = useDispatch();

  const [productGroupData, setProductGroupData] = React.useState([]);
  const [productData, setProductData] = React.useState([]);

  React.useEffect(() => {
    loadProductGroup();
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      productId: 0,
      productGroupId: 0,
    },
    validate: (values) => {
      const errors = {};

      // if (!values.productGroupId) {
      //   errors.productGroupId = "Required";
      // }

      return errors;
    },
    onSubmit: (values) => {
      //alert(JSON.stringify(values, null, 2));
      let objpayload = {
        ...stockReducer.stockPaginated,
        page: 1,
        recordsPerPage: 10,
        orderingField: "",
        ascendingOrder: true,
        productGroupId: parseInt(values.productGroupId),
        productId: parseInt(values.productId),
      };

      dispatch(stockRedux.actions.setStockPaginated(objpayload));
    },
  });

  React.useEffect(() => {
    formik.values.productId = 0;
    if (formik.values.productGroupId !== 0) {
      loadProductByProductGroupId();
    } else {
      setProductData([]);
    }
  }, [formik.values.productGroupId]);

  const loadProductGroup = () => {
    ProductAxios.getProductGroup()
      .then((res) => {
        if (res.data.isSuccess) {
          setProductGroupData(res.data.data);
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const loadProductByProductGroupId = () => {
    ProductAxios.getProductByProductGroupId(formik.values.productGroupId)
      .then((res) => {
        debugger;
        if (res.data.isSuccess) {
          setProductData(res.data.data);
        } else {
          alert(res.data.message);
        }
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const handleAdd = () => {
    dispatch(stockRedux.actions.resetStockPaginated());
  };

  return (
    <div className={classes.root}>
      <Paper elevation={3} className={classes.paper}>
        <Grid container spacing={3}>
          <Grid item xs={8} sm={4}>
            <DropDown
              name="productGroupId"
              label="productGroupId"
              formik={formik}
              defaultMenu={false}
              defaultValue={formik.values.productGroupId}
              defaultselect="ทั้งหมด"
              data={productGroupData}
            ></DropDown>
          </Grid>

          <Grid item xs={8} sm={4}>
            <DropDown
              name="productId"
              label="productId"
              formik={formik}
              defaultMenu={false}
              defaultValue={formik.values.productId}
              defaultselect="ทั้งหมด"
              data={productData}
            ></DropDown>
          </Grid>
        </Grid>
        {JSON.stringify(formik.values)}
        <Grid
          container
          spacing={2}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={12} lg={3}>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              onClick={formik.handleSubmit}
            >
              ค้นหา
            </Button>
          </Grid>
          <Grid item xs={12} lg={3}>
            <StockNew submit={handleAdd.bind(this)}></StockNew>
          </Grid>
        </Grid>
        {/* {JSON.stringify(formik.values)} */}
      </Paper>
    </div>
  );
}

export default StockSearch;
