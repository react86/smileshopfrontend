require("dayjs/locale/th");
var dayjs = require("dayjs");
dayjs.locale("th");

export const actionTypes = {
  // ADD_PLAYER: '[Add player] Action',
  //SET_CURRENTPAGE: "[SET_CURRENTPAGE] Action",

  SET_STOCKPAGINATED: "[SET_STOCKPAGINATED] Action",
  RESET_STOCKPAGINATED: "[RESET_STOCKPAGINATED] Action",
  ADD_STOCKDETAIL: "[ADD_STOCKDETAIL] Action",
  RESET_STOCKDETAIL: "[RESET_STOCKDETAIL] Action",
};

// state ค่าที่ถูกเก็บไว้
const initialState = {
  stockPaginated: {
    page: 1,
    recordsPerPage: 10,
    orderingField: "",
    ascendingOrder: true,
    productGroupId: 0,
    productId: 0,
  },

  stockDetail: {
    productGroupId: 0,
    productId: 0,
    stockTypeId: 0,
    amountStock: 0,
    amountBefore: 0,
    amountAfter: 0,
    remark: "",
  },
};

// reducer แต่ละ Action จะไป update State อย่างไร
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_STOCKPAGINATED: {
      return { ...state, stockPaginated: action.payload };
    }

    case actionTypes.RESET_STOCKPAGINATED: {
      return { ...state, stockPaginated: initialState.stockPaginated };
    }

    case actionTypes.ADD_STOCKDETAIL: {
      return { ...state, stockDetail: action.payload };
    }

    case actionTypes.RESET_STOCKDETAIL: {
      return { ...state, stockDetail: initialState.stockDetail };
    }

    default:
      return state;
  }
};

//action เอาไว้เรียกจากข้างนอก เพื่อเปลี่ยน state
export const actions = {
  setStockPaginated: (payload) => ({
    type: actionTypes.SET_STOCKPAGINATED,
    payload,
  }),

  resetStockPaginated: () => ({
    type: actionTypes.RESET_STOCKPAGINATED,
  }),

  addStockDetail: (payload) => ({
    type: actionTypes.ADD_STOCKDETAIL,
    payload,
  }),

  resetStockDetail: () => ({
    type: actionTypes.RESET_STOCKDETAIL,
  }),
};
