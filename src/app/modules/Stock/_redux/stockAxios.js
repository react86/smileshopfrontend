import axios from "axios";
import * as CONST from "../../../../Constants";
import { encodeURLWithParams } from "../../Common/components/ParamsEncode";

const Stock_URL = `${CONST.API_URL}/Stock`;
const Product_URL = `${CONST.API_URL}/Product`;

export const Filter = (
  orderingField,
  ascendingOrder,
  page,
  recordsPerPage,
  ProductGroupId,
  ProductId
) => {
  debugger;
  let payload = {
    page,
    recordsPerPage,
    orderingField,
    ascendingOrder,
  };

  if (ProductGroupId !== 0) {
    payload.ProductGroupId = ProductGroupId;
  }

  if (ProductId !== 0) {
    payload.ProductId = ProductId;
  }

  return axios.get(encodeURLWithParams(`${Stock_URL}/filter`, payload));
};

//Get Product
export const getProduct = (id) => {
  debugger;
  return axios.get(`${Product_URL}/getproduct/${id}`);
};

//Add Stock
export const addStock = (objPayload) => {
  debugger;
  return axios.post(`${Stock_URL}/addstock`, objPayload);
};

// export const getProductGroup = (id) => {
//   return axios.get(`${ProductGroup_URL}/getProductGroupbyId/${id}`);
// };

// export const updateProductGroup = (payload) => {
//   debugger;
//   return axios.put(`${ProductGroup_URL}/updateProductGroup`, payload);
// };
