/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-restricted-imports */

import React from "react";
import { TextField, MenuItem } from "@material-ui/core";
import PropTypes from "prop-types";

import FormHelperText from "@material-ui/core/FormHelperText";

function DropDown(props) {
  return (
    <div>
      <TextField
        name={props.name}
        label={props.label}
        fullWidth
        select
        onChange={(event) => {
          props.formik.setFieldValue(props.name, event.target.value);
        }}
        value={props.defaultValue}
      >
        <MenuItem disabled={props.defaultMenu} value={0}>
          {props.defaultselect}
        </MenuItem>

        {props.data.map((item) => (
          <MenuItem
            key={`${props.name}_${item[`${props.valueItem}`]}`}
            value={item[`${props.valueItem}`]}
          >
            {item[`${props.valueName}`]}
          </MenuItem>
        ))}
      </TextField>
      {props.formik.errors[`${props.name}`] &&
        props.formik.touched[`${props.name}`] && (
          <FormHelperText>
            {props.formik.errors[`${props.name}`]}
          </FormHelperText>
        )}
    </div>
  );
}

DropDown.propTypes = {
  formik: PropTypes.object,
  name: PropTypes.string,
  label: PropTypes.string,
  defaultMenu: PropTypes.bool,
  defaultselect: PropTypes.string,
  valueItem: PropTypes.string,
  valueName: PropTypes.string,
  data: PropTypes.array,
};

// Same approach for defaultProps too
DropDown.defaultProps = {
  formik: PropTypes.object,
  name: "",
  label: "",
  defaultMenu: true,
  defaultselect: "--กรุณาเลือก--",
  valueItem: "id",
  valueName: "name",
  data: [{ id: 0, name: "Do not forget to set data" }],
};

export default DropDown;
